<?php

class PressReleaseHelperTest extends \Codeception\TestCase\Test
{
    private $helper;
    protected $tester; /** @var $tester \UnitTester */

    protected function _before()
    {
        $mockSecurityContext = $this->getMockSecurityContext();

        $this->helper = $this->getPressReleaseHelper($mockSecurityContext);
    }

    protected function _after()
    {
        $this->helper = null;
    }

    // tests
    public function testInstantiation_WithValidDependencies_ShouldReturnValidObject()
    {
        $this->assertInstanceOf(
            'Newspress\PressReleaseBundle\Service\PressReleaseHelper',
            $this->helper
        );
    }

    public function testHasPermissionsToSeeThisPressRelease_WithPublicPressRelease_ReturnsTrue()
    {
        $pressRelease = $this->getPressReleaseWithRegion('now', 'Europe/London');

        $this->assertTrue(
            $this->helper->hasPermissionsToSeeThisPressRelease($pressRelease)
        );
    }

    public function testHasPermissionsToSeeThisPressRelease_WithNonePublicPressReleaseForNotLoggedInUser_ReturnsFalse()
    {
        $pressRelease = $this->getPressReleaseWithRegion('now', 'Europe/London');
        $pressRelease->setResourceType(\Newspress\PressReleaseBundle\Entity\PressRelease::RESOURCE_TYPE_ADMINISTRATIVE);

        $context = $this->getMockSecurityContextWithDefinedGrantedValue(false);

        $this->assertFalse(
            $this->getPressReleaseHelper($context)->hasPermissionsToSeeThisPressRelease($pressRelease)
        );
    }

    public function testHasPermissionsToSeeThisPressRelease_WithPressReleaseWithUnrecognisedResourceType_ReturnsFalse()
    {
        $pressRelease = $this->getPressReleaseWithRegion('now', 'Europe/London');
        $pressRelease->setResourceType('bad_value');

        $context = $this->getMockSecurityContext();

        $this->assertFalse(
            $this->getPressReleaseHelper($context)->hasPermissionsToSeeThisPressRelease($pressRelease)
        );
    }

    public function testHasPermissionsToSeeThisPressRelease_WithPressReleaseForRegisteredUsersOnly_ReturnsTrue()
    {
        $pressRelease = $this->getPressReleaseWithRegion('now', 'Europe/London');
        $pressRelease->setResourceType(\Newspress\PressReleaseBundle\Entity\PressRelease::RESOURCE_TYPE_REGISTERED);

        $context = $this->getMockSecurityContextWithDefinedGrantedValue(true);

        $this->assertTrue(
            $this->getPressReleaseHelper($context)->hasPermissionsToSeeThisPressRelease($pressRelease)
        );
    }

    public function testHasPermissionsToSeeThisPressRelease_WithAdministrativePressReleaseWithoutAdminUser_ReturnsFalse()
    {
        $pressRelease = $this->getPressReleaseWithRegion('now', 'Europe/London');
        $pressRelease->setResourceType(\Newspress\PressReleaseBundle\Entity\PressRelease::RESOURCE_TYPE_ADMINISTRATIVE);

        $context = $this->getMockSecurityContext();

        $context
            ->expects($this->exactly(2))
            ->method('isGranted')
            ->will($this->onConsecutiveCalls(true, false));

        $this->assertFalse(
            $this->getPressReleaseHelper($context)->hasPermissionsToSeeThisPressRelease($pressRelease)
        );
    }

    public function testHasPermissionsToSeeThisPressRelease_WithAdministrativePressReleaseWithAdminUser_ReturnsTrue()
    {
        $pressRelease = $this->getPressReleaseWithRegion('now', 'Europe/London');
        $pressRelease->setResourceType(\Newspress\PressReleaseBundle\Entity\PressRelease::RESOURCE_TYPE_ADMINISTRATIVE);

        $context = $this->getMockSecurityContext();

        $context
            ->expects($this->exactly(2))
            ->method('isGranted')
            ->will($this->onConsecutiveCalls(true, true));

        $this->assertTrue(
            $this->getPressReleaseHelper($context)->hasPermissionsToSeeThisPressRelease($pressRelease)
        );
    }

    public function testHasPermissionsToSeeThisPressRelease_WithEmbargoedPressReleaseWithoutCorrectRole_ReturnsFalse()
    {
        $pressRelease = $this->getPressReleaseWithRegion('now', 'Europe/London');
        $pressRelease->setResourceType(\Newspress\PressReleaseBundle\Entity\PressRelease::RESOURCE_TYPE_ADMINISTRATIVE);

        $context = $this->getMockSecurityContext();

        $context
            ->expects($this->exactly(2))
            ->method('isGranted')
            ->will($this->onConsecutiveCalls(true, false));

        $this->assertFalse(
            $this->getPressReleaseHelper($context)->hasPermissionsToSeeThisPressRelease($pressRelease)
        );
    }

    public function testHasPermissionsToSeeThisPressRelease_WithEmbargoedPressReleaseWithCorrectRole_ReturnsTrue()
    {
        $pressRelease = $this->getPressReleaseWithRegion('now', 'Europe/London');
        $pressRelease->setResourceType(\Newspress\PressReleaseBundle\Entity\PressRelease::RESOURCE_TYPE_ADMINISTRATIVE);

        $context = $this->getMockSecurityContext();

        $context
            ->expects($this->exactly(2))
            ->method('isGranted')
            ->will($this->onConsecutiveCalls(true, true));

        $this->assertTrue(
            $this->getPressReleaseHelper($context)->hasPermissionsToSeeThisPressRelease($pressRelease)
        );
    }

    /**
     * @dataProvider provider_testIsValidPressReleaseForRegionTimeZone_WithTimeBeforePublishedAt_ReturnsFalse
     */
    public function testIsValidPressReleaseForRegionTimeZone_WithTimeBeforePublishedAt_ReturnsFalse()
    {
        $pressRelease = $this->getPressReleaseWithRegion('+1hours', 'Europe/London');

        $this->assertFalse(
            $this->helper->isValidPressReleaseForRegionTimeZone($pressRelease)
        );
    }

    public function provider_testIsValidPressReleaseForRegionTimeZone_WithTimeBeforePublishedAt_ReturnsFalse()
    {
        return array(
            array('+1hours', 'Europe/London'),
            array('+4days', 'Asia/Pyongyang'),
            array('+1seconds', 'America/Havana'),
        );
    }

    /**
     * @dataProvider provider_testIsValidPressReleaseForRegionTimeZone_WithTimeLaterThanPublishedAt_ReturnsTrue
     */
    public function testIsValidPressReleaseForRegionTimeZone_WithTimeLaterThanPublishedAt_ReturnsTrue($publishedAt, $timeZone)
    {
        $pressRelease = $this->getPressReleaseWithRegion($publishedAt, $timeZone);

        $this->assertTrue(
            $this->helper->isValidPressReleaseForRegionTimeZone($pressRelease)
        );
    }

    public function provider_testIsValidPressReleaseForRegionTimeZone_WithTimeLaterThanPublishedAt_ReturnsTrue()
    {
        return array(
            array('-1hours', 'Europe/London'),
            array('now', 'Europe/London'),
            array('-1seconds', 'Australia/Adelaide'),
            array('now', 'Asia/Damascus'),
        );
    }

    private function getPressReleaseWithRegion($publishedAt, $timeZone, $resourceType = \Newspress\PressReleaseBundle\Entity\PressRelease::RESOURCE_TYPE_PUBLIC)
    {
        $region = new \Newspress\ClientBundle\Entity\Region();
        $region->setTimeZone($timeZone);
        $regions = new \Doctrine\Common\Collections\ArrayCollection(array($region));

        $pressRelease = new \Newspress\PressReleaseBundle\Entity\PressRelease();
        $pressRelease
            ->setResourceType($resourceType)
            ->setRegions($regions)
            ->setPublishedAt(new \DateTime($publishedAt))
        ;

        return $pressRelease;
    }

    /**
     * @return mixed|object|PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockSecurityContext()
    {
        return $this->getMockBuilder('Symfony\Component\Security\Core\SecurityContextInterface')
            ->disableOriginalConstructor()
            ->getMock()
        ;
    }

    /**
     * @param $mockSecurityContext
     * @return \Newspress\PressReleaseBundle\Service\PressReleaseHelper
     */
    private function getPressReleaseHelper($mockSecurityContext)
    {
        return new \Newspress\PressReleaseBundle\Service\PressReleaseHelper($mockSecurityContext);
    }

    /**
     * @return mixed|object|PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockSecurityContextWithDefinedGrantedValue($bool)
    {
        $context = $this->getMockSecurityContext();

        $context
            ->expects($this->once())
            ->method('isGranted')
            ->will($this->returnValue($bool));

        return $context;
    }
}