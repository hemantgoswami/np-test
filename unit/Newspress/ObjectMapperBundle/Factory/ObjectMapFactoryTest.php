<?php

class ObjectMapFactoryTest extends \Codeception\TestCase\Test
{
    private $factory;
    private $em;
    protected $tester; /** @var $tester \UnitTester */

    protected function _before()
    {
        $this->factory = $this->tester->grabServiceFromContainer('newspress.object_mapper.factory.object_map_factory');
        $this->em = $this->tester->grabServiceFromContainer('doctrine.orm.entity_manager');
    }

    protected function _after()
    {
        $this->em = null;
        $this->factory = null;
    }

    // tests
    public function testFactoryInstantiation_WithValidDependencies_ShouldReturnValidObject()
    {
        $this->assertInstanceOf(
            'Newspress\ObjectMapperBundle\Factory\ObjectMapFactory',
            $this->factory
        );
    }

    /**
     * @expectedException ErrorException
     */
    public function testCreateObjectMap_WithInvalidObjects_ShouldThrow()
    {
        $this->factory->createObjectMap('a', 'b');
    }

    public function testCreateObjectMap_WithValidObjects_ShouldCreateNewObjectMapAndReturn()
    {
        $mediaIdentifierType = new \Newspress\MediaBundle\Entity\MediaIdentifierType('a','b');
        $mediaIdentifier = new \Newspress\MediaBundle\Entity\MediaIdentifier($mediaIdentifierType); /** @var $mediaIdentifier \Newspress\ObjectMapperBundle\Entity\ObjectMappable */
        $client = $this->getClientByName('Infiniti'); /** @var $client \Newspress\ObjectMapperBundle\Entity\ObjectMappable */

        $this->assertFalse($this->factory->isExistingObjectMap($mediaIdentifier, $client));

        $map = $this->factory->createObjectMap($client, $mediaIdentifier); /** @var $map \Newspress\ObjectMapperBundle\Entity\ObjectMap */

        $this->assertInstanceOf(
            'Newspress\ObjectMapperBundle\Entity\ObjectMap',
            $map
        );

        $this->assertEquals(
            'Newspress\ClientBundle\Entity\Client',
            $map->getClassA()
        );

        $this->assertEquals(
            $client->getId(),
            $map->getClassAForeignKey()
        );

        $this->assertEquals(
            'Newspress\MediaBundle\Entity\MediaIdentifier',
            $map->getClassB()
        );

        $this->assertEquals(
            $mediaIdentifier->getId(),
            $map->getClassBForeignKey()
        );
    }

    public function testCreateObjectMap_WithObjectsThatHaveExistingMappedRelationship_ShouldReturnExistingMap()
    {
        $pressRelease = $this->getPressReleaseByTitle('Infiniti - PR3 - Press Release Title');

        $mediaIdentifier = $this->getMediaIdentifierByName('carousel 1');

        $this->assertTrue($this->factory->isExistingObjectMap($mediaIdentifier, $pressRelease));

        $map = $this->factory->createObjectMap($mediaIdentifier, $pressRelease); /** @var $map \Newspress\ObjectMapperBundle\Entity\ObjectMap */

        $this->assertInstanceOf(
            'Newspress\ObjectMapperBundle\Entity\ObjectMap',
            $map
        );

        // inverted, so we know for sure this wasn't created this time
        $this->assertEquals(
            get_class($pressRelease),
            $map->getClassA()
        );

        $this->assertEquals(
            $pressRelease->getId(),
            $map->getClassAForeignKey()
        );

        $this->assertEquals(
            get_class($mediaIdentifier),
            $map->getClassB()
        );

        $this->assertEquals(
            $mediaIdentifier->getId(),
            $map->getClassBForeignKey()
        );
    }

    private function getPressReleaseByTitle($title)
    {
        return $this->em->getRepository('NewspressPressReleaseBundle:PressRelease')->findOneBy(array('title'=>$title));
    }

    private function getClientByName($name)
    {
        return $this->em->getRepository('ClientBundle:Client')->findOneBy(array('name'=>$name));
    }

    private function getMediaIdentifierByName($name)
    {
        return $this->em->getRepository('NewspressMediaBundle:MediaIdentifier')->findOneBy(array('name'=>$name));
    }
}