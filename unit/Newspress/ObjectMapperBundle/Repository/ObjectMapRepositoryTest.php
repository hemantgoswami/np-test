<?php

class ObjectMapRepositoryTest extends \Codeception\TestCase\Test
{
    private $repo;
    private $em;
    protected $tester; /** @var $tester \UnitTester */

    protected function _before()
    {
        $this->repo = $this->tester->grabServiceFromContainer('newspress.object_mapper.repository.object_map_repository');
        $this->em = $this->tester->grabServiceFromContainer('doctrine.orm.entity_manager');
    }

    protected function _after()
    {
        $this->repo = null;
        $this->em = null;
    }

    // tests
    public function testRepoInstantiation_WithValidDependencies_ShouldReturnValidObject()
    {
        $this->assertInstanceOf(
            'Newspress\ObjectMapperBundle\Repository\ObjectMapRepository',
            $this->repo
        );
    }

    public function testGetRelations_WithObjectWithNoRelations_ReturnsEmptyArrayCollection()
    {
        $result = $this->repo->getRelations(new \Newspress\PressReleaseBundle\Entity\PressRelease());

        $this->assertInstanceOf(
            'Doctrine\Common\Collections\ArrayCollection',
            $result
        );

        $this->assertEmpty($result->toArray());
    }

    public function testGetRelations_WithMediaIdentifierWithOneRelationOnPrimarySide_ReturnsRelatedObject()
    {
        $mediaIdentifier = $this->getMediaIdentifierByName('carousel 1');

        $result = $this->repo->getRelations($mediaIdentifier);

        $this->assertCount(1, $result->toArray());
    }

    public function testGetRelations_WithMediaIdentifierWithOneRelationOnInverseSide_ReturnsRelatedObject()
    {
        $mediaIdentifier = $this->getMediaIdentifierByName('other 2 - test placeholders 1');

        $result = $this->repo->getRelations($mediaIdentifier);

        $this->assertCount(1, $result->toArray());
    }

    public function testGetRelations_WithMediaIdentifierOnPrimarySideWithMultipleRelations_ReturnsRelatedObjects()
    {
        $mediaIdentifier = $this->getMediaIdentifierByName('maserati gallery 1');

        $result = $this->repo->getRelations($mediaIdentifier);

        $this->assertCount(2, $result->toArray());
    }

    public function testGetRelations_WithMediaIdentifierOnInverseSideWithMultipleRelations_ReturnsRelatedObjects()
    {
        $mediaIdentifier = $this->getMediaIdentifierByName('maserati gallery 2');

        $result = $this->repo->getRelations($mediaIdentifier);

        $this->assertCount(2, $result->toArray());
    }

    public function testGetRelations_WithMediaIdentifierWithMultipleRelationsOnEitherSide_ReturnsRelatedObjects()
    {
        $mediaIdentifier = $this->getMediaIdentifierByName('infiniti gallery 1');

        $result = $this->repo->getRelations($mediaIdentifier);

        $this->assertCount(2, $result->toArray());
    }

    public function testAreRelated_WithTwoUnrelatedObjects_ReturnsFalse()
    {
        $model = $this->getModelByName('Eco Model A');
        $mediaIdentifier = $this->getMediaIdentifierByName('maserati carousel 1');

        $this->assertFalse($this->repo->areRelated($model, $mediaIdentifier));
        $this->assertFalse($this->repo->areRelated($mediaIdentifier, $model)); //inversed
    }

    public function testAreRelated_WithTwoRelatedObjects_ReturnsTrue()
    {
        $mediaIdentifier = $this->getMediaIdentifierByName('maserati gallery 2');
        $pressRelease = $this->getPressReleaseByTitle('Maserati - PR6 - Press Release Title');

        $this->assertTrue($this->repo->areRelated($pressRelease, $mediaIdentifier));
        $this->assertTrue($this->repo->areRelated($mediaIdentifier, $pressRelease)); // inversed
    }

    public function testFindObjectMapByGivenObjects_WithTwoUnrelatedObjects_ReturnsFalse()
    {
        $mediaIdentifier = $this->getMediaIdentifierByName('maserati gallery 2');
        $pressRelease = $this->getPressReleaseByTitle('Infiniti - PR7 - Press Release Title');

        $this->assertFalse(
            $this->repo->findObjectMapByGivenObjects($mediaIdentifier, $pressRelease)
        );
    }

    public function testFindObjectMapByGivenObjects_WithTwoRelatedObjects_ReturnsObjectMapObject()
    {
        $mediaIdentifier = $this->getMediaIdentifierByName('maserati gallery 2');
        $pressRelease = $this->getPressReleaseByTitle('Maserati - PR6 - Press Release Title');

        $this->assertInstanceOf(
            'Newspress\ObjectMapperBundle\Entity\ObjectMap',
            $this->repo->findObjectMapByGivenObjects($mediaIdentifier, $pressRelease)
        );
    }

    public function testGetRelationsByType_WithObjectWithNoRelations_ReturnsEmptyArrayCollection()
    {
        $this->assertInstanceOf(
            'Doctrine\Common\Collections\ArrayCollection',
            $this->repo->getRelationsByType(new \Newspress\PressReleaseBundle\Entity\PressRelease(), 'anything')
        );
    }

    /**
     * @expectedException ErrorException
     */
    public function testGetRelationsByType_WithStatusParamThatIsNotArray_Throws()
    {
        $this->repo->getRelationsByType(new \Newspress\PressReleaseBundle\Entity\PressRelease(), 'anything', 'not an array');
    }

    public function testGetRelationsByType_WithInvalidType_ReturnsEmptyArrayCollection()
    {
        $pressRelease = $this->getPressReleaseByTitle('Maserati - PR6 - Press Release Title');

        $relations = $this->repo->getRelationsByType($pressRelease, 'anything');

        $this->assertInstanceOf(
            'Doctrine\Common\Collections\ArrayCollection',
            $relations
        );

        $this->assertEmpty($relations->toArray());
    }

    public function testGetRelationsByType_WithValidType_ReturnsExpectedArrayCollection()
    {
        $model = $this->getModelByName('Coupe 001');

        $relations = $this->repo->getRelationsByType($model, 'gallery');

        $this->assertCount(1, $relations->toArray());
    }

    public function testGetRelationsByType_WithValidTypeThatHasNothingMatchingStatus_ReturnsEmptyArrayCollection()
    {
        $model = $this->getModelByName('Coupe 001');

        $relations = $this->repo->getRelationsByType($model, 'gallery', array('draft'));

        $this->assertCount(0, $relations->toArray());
    }

    /**
     * @dataProvider providerGetRelationsByType_WithValidTypeThatHasItemsMatchingStatus_ReturnsExpectedArrayCollection
     */
    public function testGetRelationsByType_WithValidTypeThatHasItemsMatchingStatus_ReturnsExpectedArrayCollection($type, $status, $count)
    {
        $pressRelease = $this->getPressReleaseByTitle('Infiniti - PR9 - Press Release Title');

        $relations = $this->repo->getRelationsByType($pressRelease, $type, $status);

        $this->assertCount($count, $relations->toArray());
    }

    public function providerGetRelationsByType_WithValidTypeThatHasItemsMatchingStatus_ReturnsExpectedArrayCollection()
    {
        return array(
            array('gallery', array('live'), 1),
            array('gallery', array('draft'), 1),
            array('gallery', array('live', 'draft'), 2),
            array('carousel', array('live'), 0),
            array('carousel', array('draft'), 0),
            array('carousel', array('live', 'draft'), 0),
        );
    }

    private function getPressReleaseByTitle($title)
    {
        return $this->em->getRepository('NewspressPressReleaseBundle:PressRelease')->findOneBy(array('title'=>$title));
    }

    private function getModelByName($name)
    {
        return $this->em->getRepository('ClientBundle:Model')->findOneBy(array('name'=>$name));
    }

    private function getMediaIdentifierByName($name)
    {
        return $this->em->getRepository('NewspressMediaBundle:MediaIdentifier')->findOneBy(array('name'=>$name));
    }
}