<?php

class ObjectMappableSubscriberTest extends \Codeception\TestCase\Test
{
    private $em;
    private $object;
    protected $tester; /** @var $tester \UnitTester */

    protected function _before()
    {
        $this->em = $this->tester->grabServiceFromContainer('doctrine.orm.entity_manager');
        $this->object = new \Newspress\ObjectMapperBundle\EventListener\ObjectMappableSubscriber();
    }

    protected function _after()
    {
        $this->em = null;
        $this->object = null;
    }

    // tests
    public function testInstantiation_ShouldReturnValidObject()
    {
        $this->assertInstanceOf(
            'Newspress\ObjectMapperBundle\EventListener\ObjectMappableSubscriber',
            $this->object
        );
    }

    public function testGetSubscribedEvents_IsListeningToCorrectEvents()
    {
        $subscribedEvents = $this->object->getSubscribedEvents();

        $this->assertCount(1, $subscribedEvents);

        $this->assertContains('preRemove', $subscribedEvents);
    }

    public function testPreRemove_WhenNotRemovingAnObjectMappable_ReturnsFalse()
    {
        $eventArgs = new \Doctrine\ORM\Event\LifecycleEventArgs(new StdClass(), $this->em);

        $this->assertFalse(
            $this->object->preRemove($eventArgs)
        );
    }

    public function testPreRemove_WhenRemovingAnObjectMap_ReturnsFalse()
    {
        $objectMappable = $this->getMockBuilder('Newspress\ObjectMapperBundle\Entity\ObjectMappable')->disableOriginalConstructor()->getMock();
        $objectMap = new \Newspress\ObjectMapperBundle\Entity\ObjectMap($objectMappable, $objectMappable);

        $eventArgs = new \Doctrine\ORM\Event\LifecycleEventArgs($objectMap, $this->em);

        $this->assertFalse(
            $this->object->preRemove($eventArgs)
        );
    }

    public function testPreRemove_WhenObjectMappableHasNoRelations_RemovesOnlyTheObjectMappable()
    {
        $brand = $this->getBrandByName('Test Delete');

        $this->assertEmpty(
            $this->getObjectMapRepository()->getRelations($brand)
        );

        $brandCount = $this->getAllBrands();
        $objectMapCount = $this->getAllObjectMaps();

        $this->em->remove($brand);
        $this->em->flush();

        $brandCountPostRemove = $this->getAllBrands();
        $objectMapCountPostRemove = $this->getAllObjectMaps();

        $this->assertEquals(count($brandCount) - 1, count($brandCountPostRemove));
        $this->assertEquals(count($objectMapCount), count($objectMapCountPostRemove));
    }

    public function testPreRemove_WhenObjectMappableHasOneRelation_RemovesObjectMappableAndObjectMap()
    {
        $brand = $this->getBrandByName('Test Delete With Relations');

        $this->assertArrayCollection($this->getObjectMapRepository()->getRelations($brand));

        $brandCount = $this->getAllBrands();
        $objectMapCount = $this->getAllObjectMaps();

        $this->em->remove($brand);
        $this->em->flush();

        $brandCountPostRemove = $this->getAllBrands();
        $objectMapCountPostRemove = $this->getAllObjectMaps();

        $this->assertEquals(count($brandCount) - 1, count($brandCountPostRemove));
        $this->assertEquals(count($objectMapCount) - 1, count($objectMapCountPostRemove));
    }

    public function testPreRemove_WhenObjectMappableHasMultipleRelations_RemovesObjectMappableAndAllRelations()
    {
        $brand = $this->getBrandByName('Test Multiple Delete With Relations');

        $this->assertArrayCollection($this->getObjectMapRepository()->getRelations($brand));

        $brandCount = $this->getAllBrands();
        $objectMapCount = $this->getAllObjectMaps();

        $this->em->remove($brand);
        $this->em->flush();

        $brandCountPostRemove = $this->getAllBrands();
        $objectMapCountPostRemove = $this->getAllObjectMaps();

        $this->assertEquals(count($brandCount) - 1, count($brandCountPostRemove));
        $this->assertEquals(count($objectMapCount) - 2, count($objectMapCountPostRemove));
    }

    private function assertArrayCollection($object)
    {
        $this->assertInstanceOf(
            '\Doctrine\Common\Collections\ArrayCollection',
            $object
        );
    }

    private function getBrandByName($name)
    {
        return $this->em->getRepository('ClientBundle:Brand')->findOneBy(array('name'=>$name));
    }

    private function getObjectMapRepository()
    {
        return $this->tester->grabServiceFromContainer('newspress.object_mapper.repository.object_map_repository');
    }

    private function getAllBrands()
    {
        return $this->em->getRepository('ClientBundle:Brand')->findAll();
    }

    private function getAllObjectMaps()
    {
        return $this->em->getRepository('NewspressObjectMapperBundle:ObjectMap')->findAll();
    }
}