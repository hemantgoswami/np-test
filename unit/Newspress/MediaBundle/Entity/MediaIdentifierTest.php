<?php

class MediaTest extends \Codeception\TestCase\Test
{
    private $em;
    protected $tester; /** @var $tester \UnitTester */

    protected function _before()
    {
        $this->em = $this->tester->grabServiceFromContainer('doctrine.orm.entity_manager');
    }

    protected function _after()
    {
        $this->em = null;
    }


    // tests
    public function testDeletingMediaIdentifier_WithEmptyMediaIdentifierCollection_OnlyDeletesEntity()
    {
        $mi = $this->getMediaIdentifierByName('test gallery 1');

        $countPre = $this->getMediaIdentifierCount();
        $collectionCountPre = $this->getMediaIdentifierCollectionCount();

        $this->em->remove($mi);
        $this->em->flush();

        $countPost = $this->getMediaIdentifierCount();
        $collectionCountPost = $this->getMediaIdentifierCollectionCount();

        $this->assertEquals(
            $countPre - 1,
            $countPost
        );

        $this->assertEquals(
            $collectionCountPre,
            $collectionCountPost
        );
    }

    public function testDeletingMediaIdentifier_WithInUseMediaIdentifierCollection_DeletesEntityAndCollection()
    {
        $mi = $this->getMediaIdentifierByName('test gallery 2');

        $countPre = $this->getMediaIdentifierCount();
        $collectionCountPre = $this->getMediaIdentifierCollectionCount();

        $this->em->remove($mi);
        $this->em->flush();

        $countPost = $this->getMediaIdentifierCount();
        $collectionCountPost = $this->getMediaIdentifierCollectionCount();

        $this->assertEquals(
            $countPre - 1,
            $countPost
        );

        $this->assertEquals(
            $collectionCountPre - 1,
            $collectionCountPost
        );

        $this->assertNull($this->getMediaIdentifierCollectionByHeading('test gallery 2 collection'));

        $this->assertImageIsNotDeleted('Infiniti Logo');
    }

    private function assertImageIsNotDeleted($imageName)
    {
        $this->assertNotNull($this->em->getRepository('NewspressMediaBundle:Media')->findOneBy(array('title'=>$imageName)));
    }

    private function getMediaIdentifierByName($name)
    {
        return $this->getMediaIdentifierRepository()->findOneBy(array('name'=>$name));
    }

    private function getMediaIdentifierCount()
    {
        return count($this->getMediaIdentifierRepository()->findAll());
    }

    private function getMediaIdentifierCollectionByHeading($heading)
    {
        return $this->getMediaIdentifierCollectionRepository()->findOneBy(array('heading'=>$heading));
    }

    private function getMediaIdentifierCollectionCount()
    {
        return count($this->getMediaIdentifierCollectionRepository()->findAll());
    }

    private function getMediaIdentifierRepository()
    {
        return $this->em->getRepository('NewspressMediaBundle:MediaIdentifier');
    }

    private function getMediaIdentifierCollectionRepository()
    {
        return $this->em->getRepository('NewspressMediaBundle:MediaIdentifierCollection');
    }
}