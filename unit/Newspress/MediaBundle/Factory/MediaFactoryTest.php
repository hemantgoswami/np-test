<?php

class MediaFactoryTest extends \Codeception\TestCase\Test
{
    private $factory;
    protected $tester; /** @var $tester \UnitTester */

    protected function _before()
    {
        $this->factory = $this->tester->grabServiceFromContainer('newspress_media.factory.media_factory');
    }

    protected function _after()
    {
        $this->factory = null;
    }

    // tests
    public function testFactoryInstantiation_WithValidDependencies_ShouldReturnValidObject()
    {
        $this->assertInstanceOf(
            'Newspress\MediaBundle\Factory\MediaFactory',
            $this->factory
        );
    }

    public function testCreate_WithInvalidMediaType_ReturnsNull()
    {
        $this->assertNull($this->factory->create('invalid'));
    }

    /**
     * @dataProvider validMediaTypeProvider
     */
    public function testCreate_WithValidMediaType_ReturnsExpectedObject($mediaType, $className)
    {
        $this->assertInstanceOf(
            $className,
            $this->factory->create($mediaType)
        );
    }

    public function validMediaTypeProvider()
    {
        return array(
            array(\Newspress\MediaBundle\Entity\MediaInterface::MEDIA_AUDIO, '\Newspress\MediaBundle\Entity\Audio'),
            array(\Newspress\MediaBundle\Entity\MediaInterface::MEDIA_DOCUMENT, '\Newspress\MediaBundle\Entity\Document'),
            array(\Newspress\MediaBundle\Entity\MediaInterface::MEDIA_IMAGE, '\Newspress\MediaBundle\Entity\Image'),
            array(\Newspress\MediaBundle\Entity\MediaInterface::MEDIA_VIDEO, '\Newspress\MediaBundle\Entity\Video'),
        );
    }
}