<?php

class MediaIdentifierFactoryTest extends \Codeception\TestCase\Test
{
    private $factory;
    protected $tester; /** @var $tester \UnitTester */

    protected function _before()
    {
        $this->factory = $this->tester->grabServiceFromContainer('newspress_media.factory.media_identifier_factory');
    }

    protected function _after()
    {
        $this->factory = null;
    }

    // tests
    public function testFactoryInstantiation_WithValidDependencies_ShouldReturnValidObject()
    {
        $this->assertInstanceOf(
            'Newspress\MediaBundle\Factory\MediaIdentifierFactory',
            $this->factory
        );
    }

    /**
     * @expectedException BadMethodCallException
     */
    public function testCreate_WithInvalidData_Throws()
    {
        $this->factory->create('aaaa');
    }

    /**
     * @dataProvider provider
     */
    public function testCreate_WithValidData_ReturnsExpectedObject($data)
    {
        $mediaIdentifier = $this->factory->create($data);

        $this->assertIsMediaIdentifier($mediaIdentifier);
        $this->assertIsExpectedMediaIdentifier($mediaIdentifier, $data);
    }

    public function provider()
    {
        return array(
            array('logo'),
            array('banner'),
            array('carousel'),
            array('icon'),
            array('gallery'),
            array('other'),
        );
    }

    public function testCreateLogo_ReturnsExpectedObject()
    {
        $mediaIdentifier = $this->factory->createLogo();

        $this->assertIsMediaIdentifier($mediaIdentifier);
        $this->assertIsExpectedMediaIdentifier($mediaIdentifier, 'logo');
    }

    public function testCreateBanner_ReturnsExpectedObject()
    {
        $mediaIdentifier = $this->factory->createBanner();

        $this->assertIsMediaIdentifier($mediaIdentifier);
        $this->assertIsExpectedMediaIdentifier($mediaIdentifier, 'banner');
    }

    public function testCreateCarousel_ReturnsExpectedObject()
    {
        $mediaIdentifier = $this->factory->createCarousel();

        $this->assertIsMediaIdentifier($mediaIdentifier);
        $this->assertIsExpectedMediaIdentifier($mediaIdentifier, 'carousel');
    }

    public function testCreateIcon_ReturnsExpectedObject()
    {
        $mediaIdentifier = $this->factory->createIcon();

        $this->assertIsMediaIdentifier($mediaIdentifier);
        $this->assertIsExpectedMediaIdentifier($mediaIdentifier, 'icon');
    }

    public function testCreateGallery_ReturnsExpectedObject()
    {
        $mediaIdentifier = $this->factory->createGallery();

        $this->assertIsMediaIdentifier($mediaIdentifier);
        $this->assertIsExpectedMediaIdentifier($mediaIdentifier, 'gallery');
    }

    public function testCreateOther_ReturnsExpectedObject()
    {
        $mediaIdentifier = $this->factory->createOther();

        $this->assertIsMediaIdentifier($mediaIdentifier);
        $this->assertIsExpectedMediaIdentifier($mediaIdentifier, 'other');
    }

    private function assertIsMediaIdentifier($object)
    {
        $this->assertInstanceOf(
            'Newspress\MediaBundle\Entity\MediaIdentifier',
            $object
        );
    }

    private function assertIsExpectedMediaIdentifier(\Newspress\MediaBundle\Entity\MediaIdentifier $mediaIdentifier, $name)
    {
        $this->assertEquals($name, $mediaIdentifier->getMediaIdentifierType()->getName());
    }

}