<?php

class MediaIdentifierCollectionFactoryTest extends \Codeception\TestCase\Test
{
    private $factory;
    protected $tester; /** @var $tester \UnitTester */

    protected function _before()
    {
        $this->factory = $this->tester->grabServiceFromContainer('newspress_media.factory.media_identifier_collection_factory');
    }

    protected function _after()
    {
        $this->factory = null;
    }

    // tests
    public function testFactoryInstantiation_WithValidDependencies_ShouldReturnValidObject()
    {
        $this->assertInstanceOf(
            'Newspress\MediaBundle\Factory\MediaIdentifierCollectionFactory',
            $this->factory
        );
    }

    /**
     * @expectedException ErrorException
     */
    public function testCreate_WithInvalidParamsValue_Throws()
    {
        $this->factory->create('invalid', 'invalid');
    }

    public function testCreate_WithValidMediaIdentifierAndMedia_ReturnsExpectedObject()
    {
        $this->assertInstanceOf(
            'Newspress\MediaBundle\Entity\MediaIdentifierCollection',
            $this->factory->create(
                $this->getMockMediaIdentifier(),
                $this->getMockMedia()
            )
        );
    }

    private function getMockMediaIdentifier()
    {
        return $this->getMockBuilder('Newspress\MediaBundle\Entity\MediaIdentifier')->disableOriginalConstructor()->getMock();
    }

    private function getMockMedia()
    {
        return $this->getMockBuilder('Newspress\MediaBundle\Entity\Media')->disableOriginalConstructor()->getMock();
    }

}