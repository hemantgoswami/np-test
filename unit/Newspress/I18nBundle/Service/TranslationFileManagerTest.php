<?php

class TranslationFileManagerTest extends \Codeception\TestCase\Test
{
    private $service;

    protected $tester; /** @var $tester \UnitTester */

    protected function _before()
    {
        $this->service = $this->tester->grabServiceFromContainer('i18n.service.translation_file_manager');
    }

    protected function _after()
    {
        $this->service = null;
    }

    // tests
    public function testServiceInstantiation_WithValidDependencies_ShouldReturnValidObject()
    {
        $this->assertInstanceOf(
            'Newspress\I18nBundle\Service\TranslationFileManager',
            $this->service
        );
    }

    public function testCreateTranslationFile_WithNewLocaleString_ShouldReturnTrue()
    {
        $this->assertTrue(
            $this->service->createTranslationFile('FAKE_FAKE')
        );
    }

    public function testCreateTranslationFile_WithExistingLocaleString_ShouldReturnTrue()
    {
        $this->assertTrue(
            $this->service->createTranslationFile('en_GB')
        );
    }

    public function testDeleteTranslationFile_WithExistingLocaleString_ShouldReturnFalse()
    {
        $this->assertFalse(
            $this->service->deleteTranslationFile('FAKE_FAKE')
        );
    }

    public function testDeleteTranslationFile_WithInvalidLocaleString_ShouldReturnFalse()
    {
        $this->assertFalse(
            $this->service->deleteTranslationFile('BAD_STRING')
        );
    }

}